import React, { Fragment } from "react";import { Route } from "react-router-dom";
import UnlimitedHosting from "../pages/HeaderLink/UnlimitedHosting";
import CloudHosting from "../pages/HeaderLink/CloudHosting";
import Afiliasi from "../pages/HeaderLink/Afiliasi";
import Domain from "../pages/HeaderLink/Domain";
import CloudVps from "../pages/HeaderLink/CloudVps";
import Blog from "../pages/HeaderLink/Blog";
import Login from "../pages/HeaderLink/Login";
import Home from "../pages/Home";
import Header from "../layout/Header"
import Footer from "../layout/Footer"
// import HomePage from "../pages/HomePage"

const Routes = () => {
    return(
      <div>
        <div className="general">
          <Header/>
            <Route path="/" component={Home} />
            <Route path="/unlimited-hosting" component={UnlimitedHosting}/>
            <Route path="/cloud-hosting" component={CloudHosting}/>
            <Route path="/afiliasi" component={Afiliasi}/>
            <Route path="/domain" component={Domain}/>
            <Route path="/cloud-vps" component={CloudVps}/>
            <Route path="/blog" component={Blog}/>
            <Route path="/login" component={Login}/>
            <Footer/>
        </div>
      </div>
    )
  }

export default Routes;