import React from "react";

const Footer = () => {
    return (
        <div className="footer" >
          <div className="container" >
            <div className="row" >
              <div className="col-12"> 
                <h5>Hubungi Kami</h5>
                <div className="foKon" >
                  <p>Telp: 0274-2885822 </p>
                  <p>WA: 0895422447394 </p>
                  <p>Senin - Minggu </p>
                  <p>24 Jam Non Stop </p>
                </div>
                <div className="foAla" >
                  <p>Jl. Palagan Tentara Pelajar </p>
                  <p>No 81 Jongkang, Sariharjo, </p>
                  <p>Ngaglik, Sleman </p>
                  <p>Daerah Istimewa Yogyakarta </p>
                  <p>55581 </p>
                </div>
              </div>
              <div className="col-12">
              <h5>Layanan</h5>
                <div className="foKon" >
                  <p>Domain</p>
                  <p>Shared Hosting</p>
                  <p>Cloud Hosting</p>
                  <p>Cloud VPS Hosting</p>
                  <p>Transfer Hosting </p>
                  <p>Web Builder </p>
                  <p>Keamanan SSL/HTTPS </p>
                  <p>Jasa Pembuatan Website </p>
                  <p>Program Affiliasi </p>
                  <p>Whois </p>
                  <p>Niagahoster Status </p>
                </div>
              </div>
              <div className="col-12">
              <h5>Service Hosting</h5>
                <div className="foKon" >
                  <p>Hosting Murah</p>
                  <p>Hosting Indonesia</p>
                  <p>Hosting Singapore SQ</p>
                  <p>Hosting Wordpress</p>
                  <p>Email Hosting</p>
                  <p>Reseller Hosting</p>
                  <p>Web Hosting Unlimited </p>
                </div>
              </div>
              <div className="col-12">
              <h5>Kenapa Pilih Niagahoster?</h5>
                <div className="foKon" >
                  <p>Hosting Terbaik</p>
                  <p>Datacenter Hosting Terbaik</p>
                  <p>Domain Gratis</p>
                  <p>Bagi-bagi Domain Gratis</p>
                  <p>Bagi-bagi Hosting Gratis</p>
                  <p>Review Pelanggan </p>
                </div>
              </div>
            </div>
            <div className="row fo" >
              <div className="col-12">
                <h5>Tutorial</h5>
                <div className="foKon" >
                  <p>Ebook Gratis</p>
                  <p>Knowledgebase</p>
                  <p>Blog</p>
                  <p>Cara Pembayaran</p>
                  <p>Niaga Course</p>
                </div>
              </div>
              <div className="col-12">
                <h5>Tentang Kami</h5>
                <div className="foKon" >
                  <p>Tentang</p>
                  <p>Penawaran dan Promo Spesial</p>
                  <p>Niaga Poin</p>
                  <p>Karir</p>
                  <p>Kontak Kami</p>
                </div>
              </div>
              <div className="col-12">
                <h5>Newsletter</h5>
                <form className="newsletter" >
                  <input className="inputNews" ></input>
                  <button className="btn-orange" >BERLANGGANAN</button>
                </form>
              </div>
            </div>
            <div className="row5" >
              <div className="row5-copy a" >
                <p>Copyright ©2019 Niagahoster | Hosting powered by PHP7, CloudLinux, CloudFlare, BitNinja and DC DCI-Indonesia. <br></br> Cloud VPS Murah powered by Webuzo Softaculous, Intel SSD and cloud computing technology</p>
              </div>
              <div className="row5-copyb" >
                <p>Syarat dan Ketentuan | Kebijakan Privasi</p>
              </div>
            </div>
          </div>
        </div>
      )
}

export default Footer;