import React from "react";
import { Link } from "react-router-dom";

const Header = () => {
    return (
        <div>
          <section className="nav">
            <div className="container">
              <ul className="nav-top">
                <li className="nav-top-item">
                  <a href="tel:0274-28885822" className="nav-top-link">
                    <i className="fas fa-phone" />
                    0274-28885822
                  </a>
                </li>
                <li className="nav-top-item">
                  <a href="/" className="nav-top-link">
                    <i className="fas fa-comment-alt" />
                    Live Chat
                  </a>
                </li>
                <li className="nav-top-item">
                  <a href="/" className="nav-top-link">
                    <i className="fas fa-shopping-cart" />
                  </a>
                </li>
              </ul>
              <nav className="nav-main">
                <a className="nav-brand" href={<Link to="/"></Link>}>
                  <img className="logo" src={require("../assets/images/nh-logo.svg")} alt="logo" />
                </a>
                <div className="nav-lists">
                  <ul>
                    <li><Link to="/unlimited-hosting" style={{ color: 'white' }}>UNLIMITED HOSTING</Link></li>
                    <li><Link to="/cloud-hosting" style={{ color: 'white' }}>CLOUD HOSTING</Link></li>
                    <li><Link to="/cloud-vps" style={{ color: 'white' }}>CLOUD VPS</Link></li>
                    <li><Link to="/domain" style={{ color: 'white' }}>DOMAIN</Link></li>
                    <li><Link to="/afiliasi" style={{ color: 'white' }}>AFILIASI</Link></li>
                    <li><Link to="/blog" style={{ color: 'white' }}>BLOG</Link></li>
                    <li><Link to="/login" style={{ color: 'white' }} >LOGIN</Link></li>
                  </ul>
                </div>
              </nav>
            </div>
          </section>
        </div>
    )
}

export default Header;