import React from "react";
import Hero from "./HomeComp/Hero";
import Services from "./HomeComp/Services";
import Priority from "./HomeComp/Priority";
import Cost from "./HomeComp/Cost";
import TestiCard from "./HomeComp/Testis";
import PackageCard from "./HomeComp/Packages";
import Clients from "./HomeComp/Clients";
import Garansi from "./HomeComp/Garansi";
import Payment from "./HomeComp/Payment";
import Sukses from "./HomeComp/Sukses";

class Home extends React.Component {
    state = {
        services: [
            {
                image: require("../assets/images/icon-1.svg"),
                name: "Unlimited Hosting",
                desc: "Cocok untuk website skala kecil dan menengah",
                price: "Rp 10.000,-"
            },
            {
                image: require("../assets/images/icons-cloud-hosting.svg"),
                name: "Cloud Hosting",
                desc: "Kapasitas resource tinggi, fully managed, dan mudah dikelola",
                price: "Rp 150.000,-"
            },            
            {
                image: require("../assets/images/icons-cloud-vps.svg"),
                name: "Cloud VPS",
                desc: "Dedicated resource dengan akses root dan konfigurasi mandiri",
                price: "Rp 104.000,-"
            },            
            {
                image: require("../assets/images/icons-domain.svg"),
                name: "Domain",
                desc: "Temukan nama domain yang Anda inginkan",
                price: "Rp 14.000,-"
            }
        ],
        
        testis: [
            {
                image: require("../assets/images/devjavu.webp"),
                desc: "Website itu sangat penting bagi UMKM sebagai sarana promosi untuk memenangkan persaingan di era digital.",
                name: "Didik & Johan",
                owner: "Owner Devjavu"
            },            
            {
                image: require("../assets/images/optimizer.webp"),
                desc: "Bagi saya Niagahoster bukan sekadar penyedia hosting, melainkan partner bisnis yang bisa dipercaya.",
                name: "Bob Setyo",
                owner: "Owner Digital Optimizer Indonesia"
            },            
            {
                image: require("../assets/images/sateratu.webp"),
                desc: ">Solusi yang diberikan tim support Niagahoster sangat mudah dimengerti buat saya yang tidak paham teknis.",
                name: "Budi Seputro",
                owner: "Owner Sate Ratu"
            }
        ],

        packages: [
            {
                label: "Termurah!",
                title: "Bayi",
                price: " ",
                disc: "Rp 10.000",
                desc: "Sesuai untuk Pemula atau Belajar Website",
                feat: (<li>
                    <ul>500 MB Disk Space</ul>
                    <ul>Unlimited Bandwidth</ul>
                    <ul>Unlimited Database</ul>
                    <ul>1 Domain</ul>
                    <ul>Instant Backup</ul>
                    <ul>Unlimited SSL Gratis Selamanya</ul>
                    <ul>Free Premium Course</ul>                    
                </li>)
            },            
            {
                label: "Diskon up to 34%",
                title: "Pelajar",
                price: "Rp. 60.800,-",
                disc: "Rp 40.223",
                desc: "Sesuai untuk Budget Minimal, Landing Page, Blog Pribadi",
                feat: (<li>
                    <ul>Unlimited Disk Space</ul>
                    <ul>Unlimited Bandwidth</ul>
                    <ul>Unlimited POP3 Email</ul>
                    <ul>Unlimited Database</ul>
                    <ul>10 Addon Domain</ul>
                    <ul>Instant Backup</ul>
                    <ul>Domain Gratis</ul>
                    <ul>Unlimited SSL Gratis Selamanya</ul>
                    <ul>Free Premium Course</ul>                    
                </li>)
            },            
            {
                label: "Diskon up to 75%",
                title: "Personal",
                price: "Rp 106.250,-",
                disc: "Rp 26.563",
                desc: "Sesuai untuk Website Bisnis, UKM, Organisasi, Komunitas, Toko Online, dll",
                feat: (<li>
                    <ul>Unlimited Disk Space</ul>
                    <ul>Unlimited Bandwidth</ul>
                    <ul>Unlimited POP3 Email</ul>
                    <ul>Unlimited Database</ul>
                    <ul>Unlimited Addon Domain</ul>
                    <ul>Instant Backup</ul>
                    <ul>Domain Gratis</ul>
                    <ul>Unlimited SSL Gratis Selamanya</ul>
                    <ul>SpamAsassin Mail Protection</ul>
                    <ul>Free Premium Course</ul>                    
                </li>)
            },
            {
                label: "Diskon up to 42%",
                title: "Bisnis",
                price: "Rp. 147.800,-",
                disc: "Rp 85.724",
                desc: "Sesuai untuk Website Bisnis, Portal Berita, Toko Online, dll",
                feat: (<li>
                    <ul>Unlimited Disk Space</ul>
                    <ul>Unlimited Bandwidth</ul>
                    <ul>Unlimited POP3 Email</ul>
                    <ul>Unlimited Database</ul>
                    <ul>Unlimited Addon Domain</ul>
                    <ul>Magic Auto Backup dan Restore</ul>
                    <ul>Domain Gratis</ul>
                    <ul>Unlimited SSL Gratis Selamanya</ul>
                    <ul>SpamAsassin Mail Protection</ul>
                    <ul>Free Premium Course</ul>                    
                </li>)
            }
        ],
        test: "test"
    }

    render () {
        return (
            <div>
                <div className="body" >
                    <Hero/>
                    <Services services={this.state.services}/>
                    <Priority/>
                    <Cost/>
                    <TestiCard testis={this.state.testis}/>
                    <PackageCard packages={this.state.packages}/>
                    <Clients/>
                    <Garansi/>
                    <Payment/>
                    <Sukses/>
                </div>
            </div>
        )
    }
}
export default Home;