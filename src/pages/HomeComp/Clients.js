import React from "react";

const Clients = () => {
    return (
        <div className="clients" >
            <div className="container" >
                <h3 className="clientsTitle">Dipercaya 52.000+ Pelanggan di Seluruh Indonesia </h3>
                <ul class="clientsList">
                    <li><span class="clientsIcon richeese"></span></li>
                    <li><span class="clientsIcon rabbani"></span></li>
                    <li><span class="clientsIcon otten"></span></li>
                    <li><span class="clientsIcon hydro"></span></li>
                    <li><span class="clientsIcon petro"></span></li>
                </ul>
            </div>
        </div>
      )
}

export default Clients;