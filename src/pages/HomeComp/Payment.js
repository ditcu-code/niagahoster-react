import React from 'react';

const Payment = () => {
  return(
    <div className="payment" >
      <div className="container" >
          <h2 className="payTitle"> Beragam pilihan cara pembayaran yang mempercepat proses order hosting & domain Anda. </h2>
          <div className="payIcon" >
              <img src={require("../../assets/images/bca.svg")} alt="bca" ></img>
              <img src={require("../../assets/images/mandiri.svg")} alt="bca" ></img>
              <img src={require("../../assets/images/bni.svg")} alt="bca" ></img>
              <img src={require("../../assets/images/bri.svg")} alt="bca" ></img>
              <img src={require("../../assets/images/bii.svg")} alt="bca" ></img>
              <img src={require("../../assets/images/cimb.svg")} alt="bca" ></img>
              <img src={require("../../assets/images/alto.svg")} alt="bca" ></img>
              <img src={require("../../assets/images/atm-bersama.svg")} alt="bca" ></img>
              <img src={require("../../assets/images/paypal.svg")} alt="bca" ></img>
              <img src={require("../../assets/images/indomart.svg")} alt="bca" ></img>
              <img src={require("../../assets/images/alfamart.svg")} alt="bca" ></img>
              <img src={require("../../assets/images/pegadaian.svg")} alt="bca" ></img>
              <img src={require("../../assets/images/pos.svg")} alt="bca" ></img>
              <img src={require("../../assets/images/ovo.svg")} alt="bca" ></img>
              <img src={require("../../assets/images/gopay.svg")} alt="bca" ></img>
              <img src={require("../../assets/images/visa.svg")} alt="bca" ></img>
              <img src={require("../../assets/images/master.svg")} alt="bca" ></img>
          </div>
      </div>
    </div>
  )
}

export default Payment;