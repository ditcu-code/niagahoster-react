import React from 'react';

const TestiCard = (props) => {
    let testi = props.testis.map(item => 
        <div className="testi-list">
            <div className="testi-detail" key={item.no} >
                <img className="testi-img" src={item.image} alt={item.name} ></img>
                <img className="testi-icon" src={require("../../assets/images/play-button.png")} alt=""></img>
                <p className="testi-desc">{item.desc} </p>
                <p className="testi-name">{item.name} - <span>{item.owner} </span></p>
            </div>
        </div>
    )

    return (
        <div>
            <section className="testi">
                <div className="container">
                    <h3 className="testi-title">Kata Pelanggan Tentang Niagahoster</h3>
                    {testi}
                </div>
            </section>
        </div>
    )
}

export default TestiCard;