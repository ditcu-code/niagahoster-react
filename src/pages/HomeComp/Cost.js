import React from 'react';


const Cost = () => {
  return(
    <div>
        <section className="cost">
            <div className="container">
                <h3 className="cost-title">Biaya Hemat, Kualitas Hebat</h3>
                <div className="pri-row">
                    <div className="cost-img">
                        <div className="cost-imgx">
                            <img className="cost-img1" src={require("../../assets/images/titis.webp")} alt="" />
                            <img className="cost-img2" src={require("../../assets/images/man.png")} alt="" />
                            <img className="cost-img3" src={require("../../assets/images/woman1.webp")} alt="" />
                            <img className="cost-img4" src={require("../../assets/images/woman2.png")} alt="" />
                            <img className="cost-img5" src={require("../../assets/images/intercom-logo.svg")} alt="" />
                            <img className="cost-img6" src={require("../../assets/images/icon-online.svg")} alt="" />
                        </div>
                    </div>
                    <div className="cost-txtcon">
                        <div className="cost-txt">
                            <div className="cost-txt1">
                                <img src={require("../../assets/images/icons-domain-harga-murah.svg")} alt="" />
                                <div className="cost-txt2">
                                    <h4 className="cost-txth4">Harga Murah, Fitur Lengkap</h4>
                                    <p className="cost-txtp">
                                        Anda bisa berhemat dan tetap mendapatkan hosting terbaik dengan
                                        fitur lengkap, dari auto install WordPress, cPanel lengkap,
                                        hingga SSL gratis{" "}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="cost-txt">
                            <div className="cost-txt1">
                                <img src={require("../../assets/images/icons-website-selalu-online.svg")} alt="" />
                                <div className="cost-txt2">
                                    <h4 className="cost-txth4">Website Selalu Online</h4>
                                    <p className="cost-txtp">
                                        Jaminan server uptime 99,98% memungkinkan website Anda selalu
                                        online sehingga Anda tidak perlu khawatir kehilangan trafik dan
                                        pendapatan.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="cost-txt">
                            <div className="cost-txt1">
                                <img src={require("../../assets/images/icons-domain-support-andal.svg")} alt="" />
                                <div className="cost-txt2">
                                    <h4 className="cost-txth4">
                                        Tim Support Andal dan Cepat Tanggap
                                    </h4>
                                    <p className="cost-txtp">
                                        Tidak perlu menunggu lama, selesaikan masalah Anda dengan cepat
                                        secara real time melalui live chat 24/7
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>;
    </div>
  )
}

export default Cost;