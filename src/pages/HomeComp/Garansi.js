import React from 'react';
import logar from "../../assets/images/icons-guarantee.svg"

const Garansi = () => {
  return(
    <div className="garansi" >
      <div className="container" >
        <div className="row2" >
          <div className="garLogo">
            <img src={logar} className="garImg" alt="logo-garansi" />
          </div>
          <div className="garText" >
            <h3 className="garTitle" >Garansi 30 Hari Uang Kembali</h3>
            <p className="garDesc" > Tidak puas dengan layanan hosting Niagahoster? Kami menyediakan garansi uang kembali yang berlaku 30 hari sejak tanggal pembelian. </p>
            <button className="btn-orange" > MULAI SEKARANG </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Garansi;