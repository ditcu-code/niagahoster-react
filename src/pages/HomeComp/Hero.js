import React from "react";
import heroImg from "../../assets/images/hero-home-ramadhan.webp";

const Hero = () => {
    return (
        <div>
            <section className="hero">
                <div className="container">
                    <div className="row">
                        <div className="hero-info">
                            <h1>Unlimited Web Hosting Terbaik di Indonesia</h1>
                            <h2>
                            Ada banyak peluang bisa Anda raih dari rumah dengan memiliki website.
                            Manfaatkan diskon hosting hingga 75% dan tetap produktif di bulan
                            Ramadhan bersama Niagahoster.
                            </h2>
                            <div className="countdown">
                                <p>Yuk segera order karena diskon dapat berakhir sewaktu-waktu!</p>
                                <ul>
                                    <li>Hari</li>
                                    <li>Jam</li>
                                    <li>Menit</li>
                                    <li>Detik</li>
                                </ul>
                            </div>
                            <a className="btn pilihSekarang" href="/">
                            PILIH SEKARANG
                            </a>
                        </div>
                        <div className="hero-img">
                            <img src={heroImg} alt="hero-home-ramadhan" />
                        </div>
                    </div>
                </div>
            </section>
        </div>
      )
}

export default Hero;