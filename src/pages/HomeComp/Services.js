import React from "react";

const ServicesCard = (props) => {
    let service = props.services.map(item => 
        <div className="ser4-card">
            <div className="ser4-detail" key={item.no} >
                <img className="ser4Icon" src={item.image} alt={item.name} ></img>
                <h4 className="ser4Title">{item.name} </h4>
                <p className="ser4Desc">{item.desc} </p>
                <p className="ser4Start">Mulai dari</p>
                <p className="ser4Price">{item.price} </p>
            </div>
        </div>
    )

    return (
        <div>
            <section className="services">
                <div className="container" >
                    <h3>Layanan Niagahoster</h3>
                    <div className="ser4">
                        {service}
                    </div>
                    <div class="ser1">
                        <div class="ser1-container">
                            <div class="ser1-card">
                                <div class="ser1-innercard">
                                    <div class="ser1-content">
                                        <div class="ser1-img x">
                                            <img src={require("../../assets/images/home-pembuatan-website.svg")} alt="ser1"></img>
                                        </div>
                                        <div class="ser1-text x">
                                            <h4>Pembuatan Website</h4>
                                            <p>500 perusahaan lebih percayakan pembuatan websitenya pada kami.<a href="https://www.niagahoster.co.id/membuat-website" title="Cek Selengkapnya" > Cek selengkapnya...</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}

export default ServicesCard;