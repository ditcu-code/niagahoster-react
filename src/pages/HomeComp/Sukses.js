import React from 'react';

const Sukses = () => {
  return(
    <div className="sukses" >
        <div className="container">
            <div className="row3" >
                <div className="sukText" >
                    <h3 > Awali kesuksesan online Anda bersama Niagahoster! </h3>
                </div>
                <div className="sukBut" >
                    <button className="btn-orange" >MULAI SEKARANG</button>
                </div>
            </div>
        </div>
    </div>
  )
}

export default Sukses;