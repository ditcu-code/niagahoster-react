import React from 'react';

const PackageCard = (props) => {
    let packaged = props.packages.map(item => 
        <div className="pacCard" >
            <div className="pacDetail" key={item.no} >
                <p className="pacLabel">{item.label} </p>
                <h5 className="pacTitle">{item.title} </h5>
                <p className="pacPrice">{item.price} </p>
                <p className="pacDisc">{item.disc} <span>/bln</span> </p>
                <button className="btn-orange" >PILIH SEKARANG</button>
                <p className="pacDesc">{item.desc} </p>
                <p className="pacFeat">{item.feat} </p>
            </div>
        </div>
    )

    return (
        <div className="package" >
            <div className="container" >
                <h3 class="package_title">Pilih Paket Hosting Anda</h3>
                {packaged}
            </div>
        </div>
    )
}

export default PackageCard;