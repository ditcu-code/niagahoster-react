import React from 'react';


const Priority = () => {
  return(
    <div>
        <section className="priority">
            <div className="container">
                <h3 className="priority-title">Prioritas Kecepatan dan Keamanan</h3>
                <div className="pri-row">
                    <div className="pri-imgcon">
                        <div className="pri-img">
                            <img className="pri-imgt" src={require("../../assets/images/server.webp")} alt="" />
                            <img className="pri-imgb" src={require("../../assets/images/graphic.svg")} alt="" />
                            <img className="pri-imgl" src={require("../../assets/images/imunify.svg")} alt="" />
                            <img className="pri-imgr" src={require("../../assets/images/lite-speed.svg")} alt="" />
                        </div>
                    </div>
                <div className="pri-txt">
                    <div className="pri-txt1">
                    <img src={require("../../assets/images/hosting-super-cepat.svg")} alt="" />
                    <div className="pri-txt2">
                        <h4 className="pri-txth4">Hosting Super Cepat</h4>
                        <p className="pri-txtp">
                        Pengunjung tidak suka website lambat. Dengan dukungan LiteSpeed
                        Web Server, waktu loading website Anda akan meningkat pesat.
                        </p>
                    </div>
                    </div>
                    <div className="pri-txt1">
                    <img src={require("../../assets/images/domain-keamanan-ekstra.svg")} alt="" />
                    <div className="pri-txt2">
                        <h4 className="pri-txth4">Keamanan Website Ekstra</h4>
                        <p className="pri-txtp">
                        Teknologi keamanan Imunify 360 memungkinkan website Anda
                        terlindung dari serangan hacker, malware, dan virus berbahaya
                        setiap saat.
                        </p>
                    </div>
                    </div>
                    <button className="btn lihatSelengkapnya">LIHAT SELENGKAPNYA </button>
                </div>
                </div>
            </div>
        </section>;
    </div>
  )
}

export default Priority;