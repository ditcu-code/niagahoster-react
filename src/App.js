import React, {Fragment} from 'react';
import Routes from "./routes/Routes";
import Header from './layout/Header';
import Footer from './layout/Footer';
import "./assets/style/hoster.css";


const App = () => {
  return(
    <div className="App" >
      <div className="general">
        <Routes/>
      </div>
    </div>
  )
}

export default App;